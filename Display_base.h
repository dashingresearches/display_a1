#ifndef Display_base_included
#define Display_base_included


#include "dTypes.h"
using namespace dTypes;

#include "Videomemory.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class tDisplay_base {

public:

  virtual ~tDisplay_base(){delete Videomemory;};

  ///////////////////////////////////////////////////////////////////////////////////
  virtual void Out_text (int arg_x0, int arg_y0,
                         int arg_x1, int arg_y1,
                         int arg_x_shift, int arg_y_shift,
                         unsigned char * argText,
                         TMemo * argDebug = 0) = 0;

  ///////////////////////////////////////////////////////////////////////////////////
  // ���������� ���������� ��������� ������ �� �����
  virtual void Set_font(class tFont* argFont = (tFont*) &Font6x8)
  {

    if(Current_font)
      Current_font = argFont;

    Height = Current_font->Height();

  };

  ///////////////////////////////////////////////////////////////////////////////////
  // ������ ������ ������� �������, ���� ����������� �����������, ������ ������� ������������
  inline int Text_width(u1x * Text)
  {

    int Sum = 0;

    while(*Text)
    {

      switch(*Text)
      {

        case '\0':
        case '\n':

          return(Sum);

      };

      Sum += Current_font->Width_for(*Text);

      Text++;

    }

    return(Sum);

  };//inline int Text_width(u1x * Text)

  //////////////////////////////////////////////////////////////////////////////
  tVideomemory * Videomemory;

protected:

  // tDisplay_base - ����������� ������� �����, ����������� ���������� ������
  // �� �������-��������
  tDisplay_base( uint X_size, uint Y_size )
  {

    Current_font = (tFont*)&Font6x8;
    Height = Current_font->Height();
   
    Videomemory = new tVideomemory( ( X_size + 7) >> 3 , Y_size);

    x_max = X_size;
    y_max = Y_size;

  };

  //////////////////////////////////////////////////////////////////////////////
  int x_max, y_max;

  //////////////////////////////////////////////////////////////////////////////
  // ������� ������ ��� �������� ����� �������� �����
  int x0, x1, y0, y1, x_shift, y_shift;
  u1x * Text;

  //////////////////////////////////////////////////////////////////////////////
  const tFont * Current_font;
  u1x Height;

};// class tDisplay_base

#endif
