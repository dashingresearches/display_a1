#ifndef dPointers_included
#define dPointers_included


namespace dPointers{

typedef void pActive(void * Context);

#define pNull ((void*)0)

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// ������ ��� �����������. ����� ���������. ������ ������ ���� ���������
// ������������� ������� ����������, �� ����������� ����, ��� ��� ������
template <class Type>
class ptr{

  public:

  mutable  Type * Data;

  inline Type* operator->(){return Data;};
  inline Type& operator*(){return *Data;};

  inline ptr& operator=(ptr& SRC){Data = SRC.Data; SRC.Data = 0; return *this;};
  inline ptr& operator=(Type * argData){Data =argData; return *this;};

  inline operator Type*(){return Data;};

  inline ptr(const ptr & SRC){Data = SRC.Data; SRC.Data = 0; };

  inline ptr( Type& argData){Data = &argData;};

  inline ptr(Type * argData){Data = argData;};

  inline ptr(){Data = 0;};

  inline ~ptr(){if(this->Data)delete this->Data;};

};













};//namespace dTypes


#endif