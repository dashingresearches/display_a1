object Form1: TForm1
  Left = 23
  Top = 205
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'automatons.demo.1'
  ClientHeight = 728
  ClientWidth = 1243
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1243
    Height = 728
    ActivePage = TabSheet1
    Align = alClient
    TabIndex = 0
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1044#1080#1089#1087#1083#1077#1081
      object Image1: TImage
        Left = 8
        Top = 32
        Width = 1025
        Height = 257
      end
      object Label1: TLabel
        Left = 16
        Top = 626
        Width = 49
        Height = 20
        Caption = #1058#1077#1082#1089#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 11
        Top = 294
        Width = 19
        Height = 20
        Caption = 'x0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 11
        Top = 342
        Width = 19
        Height = 20
        Caption = 'x1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 11
        Top = 404
        Width = 19
        Height = 20
        Caption = 'y0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 11
        Top = 452
        Width = 19
        Height = 20
        Caption = 'y1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = -2
        Top = 514
        Width = 58
        Height = 20
        Caption = 'X_shift'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = -2
        Top = 562
        Width = 58
        Height = 20
        Caption = 'Y_shift'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 427
        Top = 6
        Width = 61
        Height = 20
        Caption = #1064#1088#1080#1092#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TrackBar1: TTrackBar
        Left = 88
        Top = 296
        Width = 955
        Height = 45
        Max = 256
        Min = -256
        Orientation = trHorizontal
        Frequency = 1
        Position = 4
        SelEnd = 0
        SelStart = 0
        TabOrder = 0
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar1Change
      end
      object TrackBar2: TTrackBar
        Left = 88
        Top = 405
        Width = 955
        Height = 45
        Max = 64
        Min = -64
        Orientation = trHorizontal
        Frequency = 1
        Position = 20
        SelEnd = 0
        SelStart = 0
        TabOrder = 1
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar2Change
      end
      object TrackBar3: TTrackBar
        Left = 88
        Top = 516
        Width = 955
        Height = 45
        Max = 256
        Min = -256
        Orientation = trHorizontal
        Frequency = 1
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 2
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar3Change
      end
      object Edit1: TEdit
        Left = 96
        Top = 624
        Width = 931
        Height = 28
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        Text = 'ab'
        OnKeyPress = Edit1KeyPress
      end
      object TrackBar4: TTrackBar
        Left = 88
        Top = 344
        Width = 955
        Height = 45
        Max = 256
        Min = -256
        Orientation = trHorizontal
        Frequency = 1
        Position = 200
        SelEnd = 0
        SelStart = 0
        TabOrder = 4
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar4Change
      end
      object TrackBar5: TTrackBar
        Left = 88
        Top = 454
        Width = 955
        Height = 45
        Max = 64
        Min = -64
        Orientation = trHorizontal
        Frequency = 1
        Position = 56
        SelEnd = 0
        SelStart = 0
        TabOrder = 5
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar5Change
      end
      object TrackBar6: TTrackBar
        Left = 88
        Top = 564
        Width = 955
        Height = 45
        Max = 50
        Min = -50
        Orientation = trHorizontal
        Frequency = 1
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 6
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar6Change
      end
      object Memo1: TMemo
        Left = 1050
        Top = 32
        Width = 185
        Height = 668
        Lines.Strings = (
          'Memo1')
        ScrollBars = ssVertical
        TabOrder = 7
      end
      object ComboBox1: TComboBox
        Left = 496
        Top = 1
        Width = 145
        Height = 28
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ItemHeight = 20
        ItemIndex = 0
        ParentFont = False
        TabOrder = 8
        Text = '6x8'
        OnChange = ComboBox1Change
        Items.Strings = (
          '6x8'
          '8x10'
          '8x16'
          '16x24')
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object Memo2: TMemo
        Left = 8
        Top = 8
        Width = 489
        Height = 361
        Lines.Strings = (
          'Memo2')
        TabOrder = 0
      end
      object Memo3: TMemo
        Left = 512
        Top = 8
        Width = 225
        Height = 673
        Lines.Strings = (
          'Memo2')
        TabOrder = 1
      end
      object Memo4: TMemo
        Left = 752
        Top = 8
        Width = 225
        Height = 673
        Lines.Strings = (
          'Memo2')
        TabOrder = 2
      end
    end
  end
end
