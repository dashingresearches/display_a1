#ifndef Display_A1_v1_included
#define Display_A1_v1_included

/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/


#include "dTypes.h"
using namespace dTypes;

#include <vcl.h>

#include <stdlib.h>
#include <time.h>

#include "Font.h"
#include "Font6x8.h"

#include "Shift_register.h"

#include "Videomemory.h"

#include "Display_base.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class tDisplay_A1 : public tDisplay_base
{

  public:

  //////////////////////////////////////////////////////////////////////////////
  tDisplay_A1( uint X_size, uint Y_size, uint String_buffer_Y_size);

       ///////////////////////////////////////////////////////////////////////////////////
  void Out_text (int arg_x0, int arg_y0,
                 int arg_x1, int arg_y1,
                 int arg_x_shift, int arg_y_shift,
                 unsigned char * argText,
                 TMemo * argDebug = 0);

  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  protected:

  tShift_register Shift_register;

  union unShift_register {

    u1x d1[4];

    u2x d2[2];

    u4x d4;

  };

  u1x bytes_X_size;

  //////////////////////////////////////////////////////////////////////////////
  // ������ ��� ������ ���������� ����� � �������� �����.
  u1x * Text, *Text_begin, *Text_end;

  int x_rel, y_rel;
  int x_block, y_block;

   // ������������ ��������, ����� ������� � ���������������
  int Start_line, End_line;
  u1x Start_Y;

  //////////////////////////////////////////////////////////////////////////////
  // ������ ����� ������ � ��������
  int Line_width;

  const u1x * Symbol_array;
  u1x Width, bytes_Width;

  s1x Left_shift;

  u1x Current_shift, Current_shift_next;
  u1x Current_byte,  Current_byte_next;

  u1x bytes_For_load, bytes_For_shift, bytes_For_out;
  u1x First_byte_gen, First_byte_reg;

  u1x Start_byte, Start_shift;
  u1x Left_mask, Right_mask;

  u1x * String_buffer;
  uint String_buffer_Y_size;

  TMemo * Debug;

  //////////////////////////////////////////////////////////////////////////////
  void Clear_String_buffer();
  void Fill_String_buffer();
             
  //////////////////////////////////////////////////////////////////////////////
  void Out_String_buffer_in_videomemory();

  //////////////////////////////////////////////////////////////////////////////
  // ��������� ����������� �������������������.
  inline void Control_processing ();

  //////////////////////////////////////////////////////////////////////////////
  void Out_text_block ();


  inline void Add_data()
  {

    Debug->Lines->Add(AnsiString("Width               :  ") + AnsiString(Width));
    Debug->Lines->Add(AnsiString("Line_width          :  ") + AnsiString(Line_width));

    Debug->Lines->Add(AnsiString("   ") );

    Debug->Lines->Add(AnsiString("First_byte_gen      :  ") + AnsiString(First_byte_gen));
    Debug->Lines->Add(AnsiString("First_byte_reg      :  ") + AnsiString(First_byte_reg));
    Debug->Lines->Add(AnsiString("bytes_For_load      :  ") + AnsiString(bytes_For_load));
    Debug->Lines->Add(AnsiString("bytes_For_shift     :  ") + AnsiString(bytes_For_shift));
    Debug->Lines->Add(AnsiString("bytes_For_out       :  ") + AnsiString(bytes_For_out));

    Debug->Lines->Add(AnsiString("   ") );

    Debug->Lines->Add(AnsiString("Current_shift       :  ") + AnsiString(Current_shift));
    Debug->Lines->Add(AnsiString("Current_shift_next  :  ") + AnsiString(Current_shift_next));
    Debug->Lines->Add(AnsiString("Current_byte        :  ") + AnsiString(Current_byte));
    Debug->Lines->Add(AnsiString("Current_byte_next   :  ") + AnsiString(Current_byte_next));


  }

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  inline void Symbol_t2()
  {
      // ������������ ��������, ����� ������� � ���������������
      // ������������ ����
     for(int Current_line = Start_line; Current_line < End_line; Current_line++)
     {

       u1x * Glyph_line = (u1x*)Current_font->Image_for(*Text ) + Current_line * bytes_Width;


       unShift_register Shift_register;
      
       //Shift_register.d4 = 0xffffffff;
       Shift_register.d4 = 0x0;

      if(bytes_Width == 1)

         (Shift_register.d1[3]) = *( (u1x*)Glyph_line);

       else if (bytes_Width == 2)
       {
        // (Shift_register.d2[1]) = *( (u2x*)Glyph_line);
         Shift_register.d1[3] = Glyph_line[0];
         Shift_register.d1[2] = Glyph_line[1];
       }
       else
       {

         Shift_register.d1[3] = Glyph_line[0];
         Shift_register.d1[2] = Glyph_line[1];
         Shift_register.d1[1] = Glyph_line[2];
       }

       Shift_register.d4 = Shift_register.d4 >> Current_shift;

       Shift_register.d1[3 - First_byte_reg - First_byte_gen] &= Left_mask;

       if(bytes_For_out == 1)

         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3 - First_byte_reg - First_byte_gen];

       else if(bytes_For_out == 2)
       {
         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3 - First_byte_reg - First_byte_gen];

         String_buffer [Current_line * bytes_X_size + Current_byte + 1] |= Shift_register.d1[2 - First_byte_reg - First_byte_gen];
       }
       else
       {
         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3 - First_byte_reg - First_byte_gen];

         String_buffer [Current_line * bytes_X_size + Current_byte + 1] |= Shift_register.d1[2 - First_byte_reg - First_byte_gen];

          String_buffer [Current_line * bytes_X_size + Current_byte + 2] |= Shift_register.d1[1 - First_byte_reg - First_byte_gen];

       }

  }//for(int Current_line = Start_line; Current_line < End_line; Current_line++)

      Debug->Lines->Add(AnsiString(" "));
     Debug->Lines->Add(AnsiString("-----------"));
     Debug->Lines->Add(AnsiString("T2 : ") + AnsiString((const char *)Text,1));
     Debug->Lines->Add(AnsiString("Left_shift ") + AnsiString(Left_shift));
     Add_data();
  }

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  inline void Symbol_t2a()
  {

     
      // ������������ ��������, ����� ������� � ���������������
      // ������������ ����
     for(int Current_line = Start_line; Current_line < End_line; Current_line++)
     {

       u1x * Glyph_line = (u1x*)Current_font->Image_for(*Text ) + Current_line * bytes_Width;


       unShift_register Shift_register;
      
      // Shift_register.d4 = 0xffff0000;
       Shift_register.d4 = 0x0;

      if(bytes_Width == 1)

         (Shift_register.d1[3]) = *( (u1x*)Glyph_line);

       else if (bytes_Width == 2)
       {
        // (Shift_register.d2[1]) = *( (u2x*)Glyph_line);
         Shift_register.d1[3] = Glyph_line[0];
         Shift_register.d1[2] = Glyph_line[1];
       }
       else
       {

         Shift_register.d1[3] = Glyph_line[0];
         Shift_register.d1[2] = Glyph_line[1];
         Shift_register.d1[1] = Glyph_line[2];
       }
   
       Shift_register.d4 = Shift_register.d4 >> Current_shift;

       Shift_register.d1[3 - First_byte_reg - First_byte_gen] &= Left_mask;

       if(Right_mask)
         Shift_register.d1[3 - First_byte_reg - First_byte_gen - bytes_For_out + 1] &=  Right_mask ;

       if(bytes_For_out == 1)

         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3 - First_byte_reg - First_byte_gen];

       else if(bytes_For_out == 2)
       {
         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3 - First_byte_reg - First_byte_gen];

         String_buffer [Current_line * bytes_X_size + Current_byte + 1] |= Shift_register.d1[2 - First_byte_reg - First_byte_gen];
       }
       else
       {
         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3 - First_byte_reg - First_byte_gen];

         String_buffer [Current_line * bytes_X_size + Current_byte + 1] |= Shift_register.d1[2 - First_byte_reg - First_byte_gen];

          String_buffer [Current_line * bytes_X_size + Current_byte + 2] |= Shift_register.d1[1 - First_byte_reg - First_byte_gen];

       }

  }//for(int Current_line = Start_line; Current_line < End_line; Current_line++)

     Debug->Lines->Add(AnsiString(" "));
     Debug->Lines->Add(AnsiString("-----------"));
     Debug->Lines->Add(AnsiString("T2a : ") + AnsiString((const char *)Text,1));
     Debug->Lines->Add(AnsiString("Left_shift ") + AnsiString(Left_shift));
     Debug->Lines->Add(AnsiString("Right_mask ") + AnsiString(IntToHex(Right_mask,2)));
     Add_data();

 }

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  inline void Symbol_t3()
  {

     
      // ������������ ��������, ����� ������� � ���������������
      // ������������ ����
     for(int Current_line = Start_line; Current_line < End_line; Current_line++)
     {

       u1x * Glyph_line = (u1x*)Current_font->Image_for(*Text ) + Current_line * bytes_Width;


       unShift_register Shift_register;
      
     //  Shift_register.d4 = 0xff000000;
       Shift_register.d4 = 0x0;

      if(bytes_Width == 1)

         (Shift_register.d1[3]) = *( (u1x*)Glyph_line);

       else if (bytes_Width == 2)
       {
        // (Shift_register.d2[1]) = *( (u2x*)Glyph_line);
         Shift_register.d1[3] = Glyph_line[0];
         Shift_register.d1[2] = Glyph_line[1];
       }
       else
       {

         Shift_register.d1[3] = Glyph_line[0];
         Shift_register.d1[2] = Glyph_line[1];
         Shift_register.d1[1] = Glyph_line[2];
       }                                
   
       Shift_register.d4 = Shift_register.d4 >> Current_shift;

       if(bytes_For_out == 1)

         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3];

       else if(bytes_For_out == 2)
       {
         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3];

         String_buffer [Current_line * bytes_X_size + Current_byte + 1] |= Shift_register.d1[2];
       }
       else
       {
         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3 ];

         String_buffer [Current_line * bytes_X_size + Current_byte + 1] |= Shift_register.d1[2 ];

          String_buffer [Current_line * bytes_X_size + Current_byte + 2] |= Shift_register.d1[1 ];

       }

  }//for(int Current_line = Start_line; Current_line < End_line; Current_line++)



    Debug->Lines->Add(AnsiString(" "));
     Debug->Lines->Add(AnsiString("-----------"));
    Debug->Lines->Add(AnsiString("T3 : ")+ AnsiString((const char *)Text,1));
     Add_data();
  }

  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////
  inline void Symbol_t3a()
  {

     
      // ������������ ��������, ����� ������� � ���������������
      // ������������ ����
     for(int Current_line = Start_line; Current_line < End_line; Current_line++)
     {

       u1x * Glyph_line = (u1x*)Current_font->Image_for(*Text ) + Current_line * bytes_Width;


       unShift_register Shift_register;
      
     //  Shift_register.d4 = 0xff000000;
       Shift_register.d4 = 0x0;

      if(bytes_Width == 1)

         (Shift_register.d1[3]) = *( (u1x*)Glyph_line);

       else if (bytes_Width == 2)
       {
        // (Shift_register.d2[1]) = *( (u2x*)Glyph_line);
         Shift_register.d1[3] = Glyph_line[0];
         Shift_register.d1[2] = Glyph_line[1];
       }
       else
       {

         Shift_register.d1[3] = Glyph_line[0];
         Shift_register.d1[2] = Glyph_line[1];
         Shift_register.d1[1] = Glyph_line[2];
       }                                
   
       Shift_register.d4 = Shift_register.d4 >> Current_shift;

       if(Right_mask)
         Shift_register.d1[3 - bytes_For_out + 1] &=  Right_mask ;


       if(bytes_For_out == 1)

         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3];

       else if(bytes_For_out == 2)
       {
         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3];

         String_buffer [Current_line * bytes_X_size + Current_byte + 1] |= Shift_register.d1[2];
       }
       else
       {
         String_buffer [Current_line * bytes_X_size + Current_byte] |= Shift_register.d1[3 ];

         String_buffer [Current_line * bytes_X_size + Current_byte + 1] |= Shift_register.d1[2 ];

          String_buffer [Current_line * bytes_X_size + Current_byte + 2] |= Shift_register.d1[1 ];

       }

  }//for(int Current_line = Start_line; Current_line < End_line; Current_line++)



     Debug->Lines->Add(AnsiString(" "));
     Debug->Lines->Add(AnsiString("-----------"));
     Debug->Lines->Add(AnsiString("T3a : ")+ AnsiString((const char *)Text,1));
     Add_data();
  }

  static const u1x sb_Left_mask  [8];
  static const u1x sb_Right_mask [8];

  static const u1x vm_Left_mask  [8];
  static const u1x vm_Right_mask [8];

};// class tDisplay_A1


#endif
