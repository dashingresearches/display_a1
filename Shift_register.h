#ifndef Shift_register_included
#define Shift_register_included

/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/

#include "dTypes.h"
using namespace dTypes;

#include <vector>
using namespace std;

// � ����� �������� �� ������� �������������� ���������, ����� ���� ������ ���
// �������� ��������� �������� 

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class tShift_register{

  vector <u1x> Data;

  public:

  inline uint Size(){return (Data.size());};

  ////////////////////////////////////////////////////////////////////////////////
  tShift_register(int Size = 0)
  :Data(Size)
  {
    for(int i = 0; i < Size; i++)

      Data[i] = 0;
  };



////////////////////////////////////////////////////////////////////////////////
inline void tShift_register::Shift(int Start, int Bytes_Amount, int Shift_Amount)
{

  if(Shift_Amount < 0)
  {

    while(Shift_Amount)
    {

      bool Carry = false;

      for(int i = Start + Bytes_Amount - 1; i-- != Start ;)
      {

        bool Next_carry;

        if(Data[i] & 0x80)
          Next_carry = true;
        else
          Next_carry = false;

        Data[i] = (Data[i] << 1) | Carry;

        Carry = Next_carry;

      }

      Shift_Amount++;

    }//while(Shift_Amount)

  }//if(Shift_Amount < 0)
  else
  {

    while(Shift_Amount)
    {

      u1x Carry = 0x0;

      for(int i = Start; i < (Start + Bytes_Amount) ; i++)
      {

        u1x Next_carry = 0;

        if(Data[i] & 0x1)
          Next_carry = 0x80;
        else
          Next_carry = 0x0;

        Data[i] = (Data[i] >> 1) | Carry;

        Carry = Next_carry;

      }

      Shift_Amount--;

    }//while(Shift_Amount)

  }//else //if(Amount < 0)

}//void tShift_register::Shift(u2x Amount)


  ////////////////////////////////////////////////////////////////////////////////
  inline void Clear()
  {

    for(int i = 0; i < Data.size(); i++)
    {

      Data[i] = 0;

    }

  }


  ////////////////////////////////////////////////////////////////////////////////
  inline void Load( const u1x * Source, int Start_index_in_destination, int Width)
  {

    if(Data.size() < (Start_index_in_destination + Width) )
    {

      Data.resize(Start_index_in_destination + Width, 0);

    }

    while(Width--)
    {

      Data[Start_index_in_destination + Width] = Source[Width];

    }

}


  ////////////////////////////////////////////////////////////////////////////////
  inline void Out_by_or(u1x Start_index_in_source, u1x * Destination, u1x Width)
  {

    if(Data.size() < (Start_index_in_source + Width) )
    {

      return;

    }

    while(Width--)
    {

      Destination[Width] |= Data[Start_index_in_source + Width];

    }

}

  ////////////////////////////////////////////////////////////////////////////////
  inline u1x& operator [] (u2x Index) {return Data[Index];};


  ////////////////////////////////////////////////////////////////////////////////
  tShift_register& operator = (const tShift_register& SRC);


};// class tShift_register



#endif
 