#ifndef Main_tab_included
#define Main_tab_included


/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/

// �������� ����� - ������� ��������������� �������� �������.
// ���������� �����������, �������� �����, � ������ ����������, �������
// ������������ �������� � ��.
#include <Classes.hpp>
#include <SyncObjs.hpp>



#include "Unit1.h"

#include "Videomemory.h"

#include "Display_A1_v1.h"

#include "dPointers.h"
using namespace dPointers;

// ����� ��������� ����� ����� � ����� ����������.
class tMain_tab_process: public TThread
{

  public:

  int x0;
  int y0;

  int x1;
  int y1;

  int x_shift;
  int y_shift;

  int x;
  int y;

  vector< ptr<tDisplay_base> > Displays_set;

  tDisplay_base * Current_display;

  using TThread::Synchronize;

  TEvent * Was_event;

  AnsiString Text;

  bool Let_debug_information;

  Graphics::TBitmap * Bitmap[2];

  Graphics::TBitmap * Current_bitmap;

  /////////////////////////////////////////////////////
  tMain_tab_process()
  :TThread(false)
  {

    Bitmap[0] = new Graphics::TBitmap;
    Bitmap[0]->Width = 256 * mVideomemory_pixel_size;
    Bitmap[0]->Height = 64 * mVideomemory_pixel_size;

    Bitmap[1] = new Graphics::TBitmap;
    Bitmap[1]->Width = 256 * mVideomemory_pixel_size;
    Bitmap[1]->Height = 64 * mVideomemory_pixel_size;

    Current_bitmap = Bitmap[0];

    Was_event = new TEvent(0, true, false, "Was_event" );

    Displays_set.push_back( new tDisplay_A1(256,64,24));

    Current_display = Displays_set[0];

    Form1->Image1->Canvas->CopyMode = cmSrcCopy;

    FreeOnTerminate = true;

  };

  /////////////////////////////////////////////////////
  virtual void __fastcall Execute()
  {

    while(!Terminated)
    {

      Was_event->WaitFor(INFINITE);
      Was_event->ResetEvent();

      if(Current_display)

        Synchronize(SOut);

    }

  };


  /////////////////////////////////////////////////////
  void __fastcall SOut()
  {

    Form1->Memo1->Clear();

    Current_display->Videomemory->Fill_1();
   // Current_display->Videomemory->Clear();
    Current_display->Out_text(x0, y0, x1, y1, x_shift, y_shift, Text.c_str(), Form1->Memo1);

   // if()

    Current_display->Videomemory->Draw(Bitmap[0]->Canvas);

    Form1->Image1->Canvas->Draw(0,0,Bitmap[0]);
    
    // ���������� ���� ������
    Form1->Image1->Canvas->Pen->Width = 3;
    Form1->Image1->Canvas->MoveTo(x0 * mVideomemory_pixel_size,y0 * mVideomemory_pixel_size);
    Form1->Image1->Canvas->LineTo(x0 * mVideomemory_pixel_size,y1 * mVideomemory_pixel_size);
    Form1->Image1->Canvas->LineTo(x1 * mVideomemory_pixel_size,y1 * mVideomemory_pixel_size);
    Form1->Image1->Canvas->LineTo(x1 * mVideomemory_pixel_size,y0 * mVideomemory_pixel_size);
    Form1->Image1->Canvas->LineTo(x0 * mVideomemory_pixel_size,y0 * mVideomemory_pixel_size);

  };

};


#endif

