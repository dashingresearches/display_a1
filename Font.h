#ifndef Font_included
#define Font_included



#include "dTypes.h"
using namespace dTypes;

class tFont;

class tFont_item{

friend class tFont;

  // ������ � ��������.
  u1x Width;

  // ��������� �������� ������ ������. ������ ������� ������������ ���������� �����
  // Width � Height
  const u1x * Image;

};

class tFont{

  const tFont_item * Font;

  // ������ ���� �������� ������ ���� ���������.
  u1x _Height;
  u1x _bytes_Width;

  public:

  tFont( const tFont_item * argFont, u1x argHeight, u1x arg_bytes_Width)
  {
    Font   = argFont;
    _Height = argHeight;
    _bytes_Width = arg_bytes_Width;
  };

  inline u1x Height(){return(_Height);};

  inline u1x bytes_Width(){return(_bytes_Width);};


  inline u1x Width_for(const u1x Character){return(Font[Character].Width);};

  inline const u1x * Image_for(const u1x Character){return(Font[Character].Image);};

};

#endif
