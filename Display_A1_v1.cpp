#include "Display_A1_v1.h"



/*
(�)Copyright Alexander Solovyev 2017
             ��������� �������� 2017

���� ����������� ��� � ��� ����� ����� �������� �������������� � ������� ���
������������ �����, � ������������ ������� �� ���� dashingresearches.wordpress.com

this program and it's parts are allowed for free educational and commertial use,
but the link to dashingresearches.wordpress.com must be given


*/


const u1x tDisplay_A1::sb_Left_mask[8]=\
{0xff,0x7f,0x3f,0x1f,0x0f,0x07,0x03,0x01};

const u1x tDisplay_A1::sb_Right_mask[8]=\
{0x00,0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe};

const u1x tDisplay_A1::vm_Left_mask[8]=\
{0x00,0x80,0xc0,0xe0,0xf0,0xf8,0xfc,0xfe};

const u1x tDisplay_A1::vm_Right_mask[8]=\
{0xff,0x7f,0x3f,0x1f,0x0f,0x07,0x03,0x01};



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
tDisplay_A1::tDisplay_A1( uint argX_size, uint argY_size, uint argString_buffer_Y_size)
:tDisplay_base( argX_size, argY_size ),
 Shift_register (bytes_X_size = (argX_size + 7) >> 3)
{

  String_buffer = new u1x[ bytes_X_size * argString_buffer_Y_size ];
  String_buffer_Y_size = argString_buffer_Y_size;

}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void tDisplay_A1::Out_text_block ()
{

  x_block = x0 + x_rel + x_shift;
  y_block = y0 + y_rel + y_shift;

  Text = Text_begin;


  if(y_block < y0)
  {
    Start_line = y0 - y_block;
    Start_Y = y0;
  }
  else
  {
    Start_line = 0;
    Start_Y = y_block;
  }

  if( ( Start_line >= Height) || (y1 <= y_block) )
    // ����� �� ����� � ������� ������, �� �������
    return;

  if( (y1 - y_block) >= Height)
  {
    End_line = Height;
  }
  else
  {
    End_line = y1 - y_block;
  }



  if(x_block < x0)
  {
    Left_shift = x0-x_block;
    x_block = x0;
  }
  else

    Left_shift = 0;

  Line_width = x1 - x_block;

  if(Line_width <= 0)

    return;

  bytes_Width = Current_font->bytes_Width();

////////////////////////////////////////
Type_1:

  // ���� �� ����� ������
  while(Text < Text_end)
  {

    Width = Current_font->Width_for(*Text);
      
    // ������������ ������� ���� �� ������ � ������� �����������
    if(Left_shift >= Width)
    {
      Left_shift  -= Width;
      Text++;
    }
    else

      goto Type_2;

  }// while(Text < Text_end)

  // ����� ������
  return;

 
////////////////////////////////////////
Type_2:


  Clear_String_buffer();
  Current_byte = 0;
  Start_byte = x_block >> 3;
  Start_shift = x_block & 0x7;

  // �������� �������?
  if(Line_width < ( Width - Left_shift) )
  {
    Width = Line_width + Left_shift;
    Current_shift   = x_block & 0x7;
    First_byte_gen  = Left_shift  >> 3;
    int Corrected_Left_shift = Left_shift & 0x7;
    Left_mask       = sb_Left_mask[Current_shift];
    // ������ Result_shift ������������ ���������� Current_shift

    if(Corrected_Left_shift <= Current_shift ) // Result_shift > 0
    {

      Current_shift -= Corrected_Left_shift;
      First_byte_reg = 0;

      bytes_For_shift   = ((Width + Current_shift + 7) >> 3) - First_byte_gen;
      bytes_For_out     = bytes_For_shift;

    }
    else
    {
           
      Current_shift = (Current_shift - Corrected_Left_shift ) & 0x7;
      First_byte_reg = 1;

      bytes_For_shift   = ((Width + Current_shift + 7) >> 3) - First_byte_gen;
      bytes_For_out     = bytes_For_shift - 1;

    }

    Current_shift_next = (Current_shift + Width) & 0x7;
    Current_byte_next  = Current_byte + ( (Current_shift + Width) >> 3 ) - First_byte_gen - First_byte_reg;

    Right_mask      = sb_Right_mask[ Current_shift_next ];

    Line_width = 0;

    Symbol_t2a();

    Current_shift = Current_shift_next;
    Current_byte  = Current_byte_next;

    goto Finalize;

  }


  Line_width -= (Width - Left_shift);

  Current_shift   = x_block & 0x7;
  First_byte_gen  = Left_shift  >> 3;
  int Corrected_Left_shift = Left_shift & 0x7;
  Left_mask       = sb_Left_mask[Current_shift];
  // ������ Result_shift ������������ ���������� Current_shift


  if(Corrected_Left_shift <= Current_shift ) // Result_shift > 0
  {

    Current_shift -= Corrected_Left_shift;
    First_byte_reg = 0;

    bytes_For_shift   = ((Width + Current_shift + 7) >> 3) - First_byte_gen;
    bytes_For_out     = bytes_For_shift;
        
  }
  else
  {
           
    Current_shift = (Current_shift - Corrected_Left_shift ) & 0x7;
    First_byte_reg = 1;

    bytes_For_shift   = ((Width + Current_shift + 7) >> 3 ) - First_byte_gen;
    bytes_For_out     = bytes_For_shift - 1;

  }

      
  Current_shift_next = (Current_shift + Width) & 0x7;
  Current_byte_next  = Current_byte + ( (Current_shift + Width) >> 3 ) - First_byte_gen - First_byte_reg;


  //////////////////////////////////
  //��
  //////////////////////////////////
  Symbol_t2();
  Current_shift = Current_shift_next;
  Current_byte  = Current_byte_next;

  // ����� ��������� ������
  Text++;

////////////////////////////////////////
Type_3:

  // ����� ������?
  while(Text < Text_end)
  {

    Width = Current_font->Width_for(*Text);

    // �������� �������?
    if(Line_width < Width)
    {

      if(Line_width <= 0)

        goto Finalize;

      Width = Line_width;
      Line_width = 0;
      bytes_For_shift = (Width + Current_shift + 7) >> 3;
      bytes_For_out   = bytes_For_shift;

      Current_shift_next = (Current_shift + Width) & 0x7;
      Current_byte_next  = Current_byte + ( (Current_shift + Width) >> 3 );

      Right_mask      = sb_Right_mask[ Current_shift_next ];

      Symbol_t3a();
      Current_shift = Current_shift_next;
      Current_byte  = Current_byte_next;

      goto Finalize;

    }

    Line_width -= Width;

    bytes_For_shift = (Width + Current_shift + 7) >> 3;
    bytes_For_out  = bytes_For_shift;

    Current_shift_next = (Current_shift + Width) & 0x7;
    Current_byte_next  = Current_byte + ( (Current_shift + Width) >> 3 );

    //////////////////////////////////
    //��
    //////////////////////////////////
    Symbol_t3();
    Current_shift = Current_shift_next;
    Current_byte  = Current_byte_next;

    Text++;

  }// while(Text < Text_end)

  Finalize:

  Line_width = x1 - x_block - Line_width;

  Out_String_buffer_in_videomemory();

  return;


}// void tDisplay_A1::Out_text_block ()



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay_A1::Control_processing ()
{

  if(*Text_end == '\n')
  {

    y_rel += Height;
    x_rel = 0;

    Text_end ++;

    Text_begin = Text_end;

    return;

  }

  //Text_end ++;

}// void tDisplay_A1::Control_processing ()



///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay_A1::Out_text (int arg_x0, int arg_y0,
                         int arg_x1, int arg_y1,
                         int arg_x_shift, int arg_y_shift,
                         unsigned char * argText,
                         TMemo * argDebug)
{

  x0 = arg_x0;
  x1 = arg_x1;
  y0 = arg_y0;
  y1 = arg_y1;

  x_shift = arg_x_shift;
  y_shift = arg_y_shift;

  Text = Text_begin = Text_end = argText;

  Debug = argDebug;

  if(x1 < x0)
  {
    int temp = x0;
    x0 = x1;
    x1 = temp;
  }

  if(y1 < y0)
  {
    int temp = y0;
    y0 = y1;
    y1 = temp;
  }

  if(x0 < 0)
  {
    x_shift += x0;
    x0 = 0;
  }

  if(x1 > x_max)
  {
    x1 = x_max;
  }

  if(y0 < 0)
  {
    y0 = 0;
  }

  if(y1 > y_max)
  {
    y1 = y_max;
  }

  x_rel = y_rel = 0;

  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  while(*Text_end)
  {

//////////////////////////////////////
state__Inside_text_block:

  while(1)
  {

    switch(*Text_end)
    {

      case '\0':
      case '\n':
        goto state__Out_text_block;

    }

    Text_end++;

  }

//////////////////////////////////////
state__Out_text_block:

  if(Text_begin != Text_end)
  {

    Out_text_block();

    // ����� ���������� Execute_script � Line_width ����������� ���������� ���������� ��������
    x_rel += Line_width;

  }

  Text_begin = Text_end;

//////////////////////////////////////
state__Control_processing:

  // ����� �������� � ��������� �������
  Control_processing();

  }//while(*Text_end)

}//void tDisplay_A1::Out_text (int arg_x0, int arg_y0,




////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
void tDisplay_A1::Out_String_buffer_in_videomemory()
{

  // ����� �������� ������ ��������������� � �������� ������ �����


  for(int Current_line = Start_line ; Current_line < End_line; Current_line++ )
  {

    // ����� �������� ������ �������� ���������� ����� � ���������� ���
    if(Start_shift)
    {

      Videomemory->COM__Set_coordinates(Start_byte,Start_Y + Current_line - Start_line);
      Videomemory->COM__Set_Read_mode();
      while(Videomemory->COM__Wait());

      u1x Intersection_byte_data = Videomemory->COM__Read();

      Intersection_byte_data &= vm_Left_mask[Start_shift];

      String_buffer[Current_line * bytes_X_size] |= Intersection_byte_data;

    }

    // ����� �������� ������ �������� ���������� ����� � ���������� ���
    // � Line_width �� ��������� ������ ��������� ���������� ���������� ��������
    if(Current_shift)
    {

      Videomemory->COM__Set_coordinates(Start_byte + Current_byte, Start_Y + Current_line - Start_line);
      Videomemory->COM__Set_Read_mode();
      while(Videomemory->COM__Wait());

      u1x Intersection_byte_data = Videomemory->COM__Read();

      Intersection_byte_data &= vm_Right_mask[Current_shift];

      String_buffer[Current_line * bytes_X_size + Current_byte] |= Intersection_byte_data;

    }

    Videomemory->COM__Set_coordinates(Start_byte, Start_Y + Current_line - Start_line);
    Videomemory->COM__Set_Write_mode();
    while(Videomemory->COM__Wait());

    for(uint i = 0; i < Current_byte; i++)

      Videomemory->COM__Write(String_buffer[Current_line * bytes_X_size + i]);

    if(Current_shift)

      Videomemory->COM__Write(String_buffer[Current_line * bytes_X_size + Current_byte]);

  }//for(int Current_line = Start_line ; Current_line < End_line; Current_line++ )

}//void tDisplay_A1::Out_String_buffer_in_videomemory()

                          ////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void tDisplay_A1::Clear_String_buffer()
{

  for(int i = 0; i < Height * bytes_X_size; i ++)
  {

    String_buffer[i] = 0x00;

  }

};



void tDisplay_A1::Fill_String_buffer()
{

  for(int i = 0; i < String_buffer_Y_size * bytes_X_size; i ++)
  {

    String_buffer[i] = 0x55;

  }

};

